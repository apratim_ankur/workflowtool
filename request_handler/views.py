from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from django.views.decorators.csrf import csrf_exempt

from request_handler.models import *

from request_handler.forms import *

import json

from datetime import datetime

context_dir={}
loggedin = False



@csrf_exempt
def dashboard(request,username):
	videos = None
	smes=None
	admin = ['ankur']
	loggedin = False
	current_user = None
	orders = []
	enquiries = []
	allowed = False
	loggedin_user = None
	try:
		loggedin = request.user.is_authenticated
		loggedin_user = request.user
	except:
		pass
	if loggedin:
		try:
			current_user = User.objects.get(username=username)
		except:
			current_user = None

	if request.user.username == username:
		allowed = True


	if loggedin:
		if request.user.username in admin:
			allowed = True
			if request.user.username == username:
				orders = Enquiry.objects.filter(status="approved")
				enquiries = Enquiry.objects.filter(status="unapproved")
		
		try:
			enquiries_of_this_client = Enquiry.objects.filter(client=current_user.userprofile)

		except:
			enquiries_of_this_client = None
		if enquiries_of_this_client:
			try:
				for enquiry in enquiries_of_this_client:
					if str(enquiry.status) == "approved":
						orders.append(enquiry)
					else:
						enquiries.append(enquiry)
			except:
				pass

	# return HttpResponse(allowed)
	context_dir.update({
		'orders' : orders ,
		'enquiries' : enquiries,
		'current_user' : current_user,
		'loggedin' : loggedin,
		'allowed' : allowed,
		'loggedin_user' : loggedin_user ,
		})
	# return HttpResponse(len(orders))
	return render_to_response("workflowtable.html", context_dir, context_instance=RequestContext(request))






@csrf_exempt
def signup(request, path):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        password = request.POST['password']
        verify_password = request.POST['verify_password']
        if password == verify_password:
            user = User.objects.create_user(username=username, email=email, password=password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect('/'+ str(path))
    context_dir = {}
    return render_to_response('signup.html', context_dir)

@csrf_exempt
def login_handler(request, path):
    
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/'+ str(path))
            else:
                return HttpResponse("disabled account")
        else:
            return HttpResponse("Invalid Login")
    if request.user.is_authenticated():
        return redirect('/'+ str(path))
    return render_to_response('login.html', {'form': LoginForm() })


def logout_handler(request, path):
    logout(request)
    return redirect('/'+ str(path))


@csrf_exempt
def feedback(request):
    if request.method == 'POST':
 
        try:
            feedback = request.POST['message']
        except:
            pass
        try:
            feedback = request.POST['rate']
        except:
            pass

        Feedback.objects.create(feedback=feedback, rate=rate, sender = request.user.userprofile)
        messages.info(request, 'Your Feedback Has Been Sent. Thank You.')
        return redirect('/')
        

def about(request):
	loggedin = False
	try:
		loggedin = request.user.is_authenticated
	except:
		pass
	feedbackform = FeedbackForm()
	context_dir.update({'loggedin': loggedin, 'feedbackform': feedbackform,})
	return render_to_response('about.html', context_dir, context_instance=RequestContext(request))


def get_storyboard(request, username, IndividualOrders_id):
	if request.user.is_authenticated:
		current_storyboard = IndividualOrders.objects.get(id=IndividualOrders_id).storyboard
		response_urls = []

		index = 0 

		for storyboard_frame in current_storyboard.frames.all():
			response_urls.append(storyboard_frame.file.url)
			index = index + 1		

		return HttpResponse(json.dumps(response_urls), 'application/json')

	else:

		return HttpResponse('You Need To Log In')


@csrf_exempt
def workflowtool(request):
    return render_to_response("work-tool.html", context_dir, context_instance=RequestContext(request))


@csrf_exempt
def workflowtoolapp(request):
    return render_to_response("workflowtoolapp.html", context_dir, context_instance=RequestContext(request))



@csrf_exempt
def storyboard_view(request):
	return render_to_response("storyboard_viewer.html", context_dir, context_instance=RequestContext(request))



@csrf_exempt
def enquiry_profile(request, username, enquiry_title):
	allowed = False

	try:
		selected_enquiry = Enquiry.objects.get(title= enquiry_title)
	except:
		selected_enquiry = None
	try:
		loggedin = request.user.is_authenticated
		is_staff = request.user.is_staff
	except:
		loggedin = False
		is_staff = False
	if loggedin:
		loggedin_user = request.user
		if loggedin_user.username == username:
			allowed = True
		if is_staff:
			allowed = True
	else:
		loggedin_user = None
	if selected_enquiry and loggedin:
		try:
			po = PurchaseOrder.objects.get(order=selected_enquiry)
		except:
			po = None
	context_dir.update(
		{
		'selected_enquiry' : selected_enquiry,
		'selected_enquiry_requirements' : json.dumps(selected_enquiry.requirements),
		'loggedin_user' : loggedin_user,
		'po' : po,
		'is_staff':is_staff,
		'allowed' :allowed,
		}
		)
	return render_to_response( 'enquiry_profile.html', context_dir,context_instance=RequestContext(request) )


@csrf_exempt
def create_po(request, loggedin_user, enquiry_title):
	try:
		po_no = request.POST['po_no']
	except:
		po_no = None
	try:
		selected_enquiry = Enquiry.objects.get(title=enquiry_title)
	except:
		selected_enquiry = None

	if po_no and selected_enquiry:
		new_po = PurchaseOrder.objects.create(po_no=po_no,order=selected_enquiry)
		if new_po:
			selected_enquiry.status= 'approved'
			selected_enquiry.save()
			return redirect('/enquiry_profile/' + str(loggedin_user) + "/" + str(enquiry_title))
	else:
		return render_to_response('confirm_po_modal.html', context_dir,context_instance=RequestContext(request))


@csrf_exempt
def individual_order_profile(request, username, order_title):
	comments = None
	replies = []
	approve = False
	smes = False

	try:
		loggedin = request.user.is_authenticated
	except:
		pass
	try:
		order = IndividualOrders.objects.get(title=order_title)
	except:
		raise Http404('Requested Order Could Not Be Found.')
	# return HttpResponse(order);
	comment = None
	reply=None
	if request.method == 'POST':
		try:
			comment = request.POST['comment']
		except:
			pass
		try:
			reply = request.POST['reply']
		except:
			pass
		try:
			approve = request.POST['confirm_approve']
		except:
			pass
	
	smes = Sme.objects.filter(orders = order)

	if smes:
		try:
			for sme in smes:
				if request.user.userprofile == sme.sme:
					loggedin = True
		except:
			pass

		if request.user.username == 'ankur':
			loggedin = True
	# return HttpResponse(loggedin)
	if approve:
		order.status = 'approved'
		order.save()

	if comment:
		Comment.objects.create(comment=comment, commenter= request.user.userprofile, IndividualOrders=order)

	if reply:
		Reply.objects.create(reply=reply, replier= request.user.userprofile, comment=Comment.objects.get(id=request.POST['replied_comment_id']))
	

	try:
		comments = Comment.objects.filter(IndividualOrders=order).IndividualOrders_by("-datetime_created")
	except:
		pass
	if comments:
		for e in comments:
			try:
				replies_to_e = Reply.objects.filter(comment=e)
				replies.append(replies_to_e)
			except:
				pass

	# return HttpResponse(request.user.userprofile)
	# return HttpResponse(replies[0])
	context_dir.update({'IndividualOrders':order, 'loggedin':loggedin, 'comments': comments, 'replies' :replies, })

	# return HttpResponse(comments)
	# return HttpResponse(request.path)
	return render_to_response('order_profile.html', context_dir,context_instance=RequestContext(request))


@csrf_exempt
def create_enquiry(request,enquiry_title, current_user):
	errors = {}
	if request.method == "POST":
		try:
			enquiry_title_inpost = request.POST['enquiry_title']
		except:
			enquiry_title_inpost = None
		try:
			enquiry_description = request.POST['enquiry_description']
		except:
			enquiry_description = None
		try:
			enquiry_target_user = request.POST['enquiry_target_user']
		except:
			enquiry_target_user = None
		try:
			enquiry_timeframe = request.POST['enquiry_timeframe']
		except:
			enquiry_timeframe = None
		try:
			enquiry_end_purpose = request.POST['enquiry_end_purpose']
		except:
			enquiry_end_purpose = None
		try:
			enquiry_requirements = request.POST['enquiry_requirements']
		except:
			enquiry_requirements = None
		# return HttpResponse(str(enquiry_timeframe[0]))

		try:
			new_enquiry= Enquiry.objects.create(title= enquiry_title_inpost,client= request.user.userprofile)
			# new_enquiry.timeframe= enquiry_timeframe
			new_enquiry.end_purpose= enquiry_end_purpose 
			new_enquiry.target_audience = enquiry_target_user 
			new_enquiry.description= enquiry_description
			new_enquiry.requirements= enquiry_requirements
			new_enquiry.timeframe= datetime(int(enquiry_timeframe[6:]),int(enquiry_timeframe[:2]),int(enquiry_timeframe[3:5]))
			new_enquiry.save()
				
		except:
			errors.update({"create_problem": "your enquiry couldnt be submitted"})
			return HttpResponse(enquiry_requirements)
		return HttpResponse(str(new_enquiry.title))
	else:
		return HttpResponse("nothing to post pal...!!!")


def contact(request):
	return render_to_response('contact.html', context_dir, context_instance=RequestContext(request))


def view_404(request):
	return HttpResponse("stop looking for what doesn't exist...!")