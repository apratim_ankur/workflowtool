from django.db import models

from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_save

from datetime import date, timedelta, datetime

from django.core.files import File

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin

from jsonfield.fields import JSONField

import json

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    datetime_created = models.DateTimeField(auto_now_add=True)


    def __unicode__(self):
	    return self.user.username

def create_userprofile_post_user_save(sender, instance, created, **kwargs):
    """Create a user profile when a new user account is created"""
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_userprofile_post_user_save, sender=User)

admin.site.register(UserProfile)




class Proposal(models.Model):
	quote = models.CharField(max_length=10000,null=True,blank=True)
	
	datetimes_modified_list = JSONField()

admin.site.register(Proposal)



class Enquiry(models.Model):
	title = models.CharField(max_length=1000)
	client = models.ForeignKey(UserProfile)
	timeframe = models.DateTimeField(null=True,blank=True)
	target_audience = models.CharField(max_length=200)
	end_purpose = models.CharField(max_length=200)

	requirements = JSONField( default={"liveactionvideo": 0, "2danimation" :0, "interactive":0, "3danimation":0} ) #number of each type of product required. {category1: number, category2:number,...}

	datetimes_modified_list = JSONField(default={"datetimes_modified":[]})

	enquiry_status_choices = ( 
		('unapproved','unapproved'),
		('approved' , 'approved'),
		)

	status = models.CharField(max_length=200, choices = enquiry_status_choices, default = 'unapproved')
	
	proposal = models.OneToOneField(Proposal, null=True, blank=True)

	def __unicode__(self):
	    return "title- " + str(self.title) + " , client- " + str(self.client)

admin.site.register(Enquiry)


class PurchaseOrder(models.Model):
	po_no = models.IntegerField(unique=True, null=True,blank=True)
	po_datetime_created = models.DateTimeField(auto_now_add=True)
	order = models.OneToOneField(Enquiry)

	def __unicode__(self):
	    return str(self.po_no)

admin.site.register(PurchaseOrder)


class StoryBoardFrame(models.Model):
	description = models.CharField(max_length=3000, unique=True, null=True, blank=True)
	file = models.FileField(upload_to='storyboard_frames', null=True, blank=True)

class StoryBoard(models.Model):
	frames = models.ManyToManyField(StoryBoardFrame, null= True, blank=True)

	datetimes_modified_list = JSONField()

	def update_datetime(current_time):
		self.datetime_modified = datetime.datetime.now()

	def __unicode__(self):
	    return str(self.name)
admin.site.register(StoryBoard)



class Animation(models.Model):
	animation = models.FileField(upload_to='completed_videos', null=True, blank=True)
		



class IndividualOrders(models.Model):
	description = models.CharField(max_length=3000, null=True, blank=True)
	status_choices = ( 
		('ytb','yet to begin'),
		('wip' , 'storyboard making'),
		('completed', 'completed animation'),
		('approved', 'approved'),
		)
	category_choices = ( 
		('liveactionvideo','Live Action Video'),
		('2danimation' , '2D Animation'),
		('interactive', 'Interactive Animation'),
		('3danimation', '3D Animation'),
		)

	status = models.CharField(max_length=100, choices = status_choices)
	category = models.CharField(max_length=300)
	storyboard = models.OneToOneField(StoryBoard, null=True, blank=True)
	datetime_created = models.DateTimeField(auto_now_add=True)
	animation = models.OneToOneField(Animation, null=True, blank=True)

	po = models.ForeignKey(PurchaseOrder)
 
	thumb = models.FileField(upload_to='completed_videos_thumbs', null=True, blank=True)

	def __unicode__(self):
	    return self.category

admin.site.register(IndividualOrders)




class Sme(models.Model):
	sme = models.ForeignKey(UserProfile)
	orders = models.ForeignKey(IndividualOrders)

	def __unicode__(self):
	    return unicode(self.sme)

# def sme_post_save(sender, instance, created, **kwargs):
#     """Create a user profile when a new sme account is created"""
#     if created:
#         Sme.objects.create(sme=instance)

# post_save.connect(sme_post_save, sender=UserProfile)


class SmeAdmin(admin.ModelAdmin):
	def create_sme(request,*args, **kwargs):
		if request.method == 'POST':
			try:
				sme = request.POST['sme']
			except:
				pass
			try:
				videos = request.POST['videos']

			except:
				pass

			Sme.objects.create(sme= sme, orders= videos)
		


admin.site.register(Sme, SmeAdmin)


class Comment(models.Model):
	comment = models.CharField(max_length=10000, null=True, blank=True)
	commenter = models.ForeignKey(UserProfile)
	order = models.ForeignKey(IndividualOrders)
	datetime_created = models.DateTimeField(auto_now_add=True)
	comment_status_choices = ( 
		('unresolved','unresolved'),
		('resolved' , 'resolved'),
		)
	status = models.CharField(max_length=100, choices = comment_status_choices, default = 'unresolved')

	def target(self,target):
		self.comment_target = models.ForeignKey(target)
	def __unicode__(self):
	    return str(self.comment) + ' --to video-- ' + str(self.order) + ' --by-- ' + str(self.commenter)
admin.site.register(Comment)


class Reply(models.Model):
	reply = models.CharField(max_length=10000, null=True, blank=True)
	replier = models.ForeignKey(UserProfile)
	comment = models.ForeignKey(Comment)
	datetime_created = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return str(self.reply)  + ' --to topic-- ' + str(self.comment) + ' --by-- ' + str(self.replier)

admin.site.register(Reply)


def pre_save_datetimes_modified(sender, instance, **kwargs):
	getattr(instance,"datetimes_modified_list")["datetimes_modified"].append(datetime.now())
	
pre_save.connect(pre_save_datetimes_modified, sender=Enquiry)
pre_save.connect(pre_save_datetimes_modified, sender=Proposal)
pre_save.connect(pre_save_datetimes_modified, sender=StoryBoard)


def post_save_po_create_individual_orders(sender, instance, created, **kwargs):
	if created:
		for item in instance.order.requirements:
			for i in range( 0, int(instance.order.requirements[str(item)]) ):				
				IndividualOrders.objects.create(description=str(item) ,category=str(item), status="ytb", po=instance)

post_save.connect(post_save_po_create_individual_orders, sender=PurchaseOrder)


