

storyboard = document.getElementById('storyboard_modal');
storyboard_center = document.getElementById('storyboard_canvas');
storyboard_nav = document.getElementById('storyboard_nav');
storyboard_comments = document.getElementById('storyboard_comments')

function remove_storyboard_comment () {
  console.log(this);
  this.parentNode.parentNode.removeChild(this);
}

function add_comment (event) {

  var x_pos = event.offsetX
  var y_pos = event.offsetY
  console.log(x_pos);
  console.log(y_pos);
  comment_container = document.createElement('div');
  tools_container = document.createElement('div');
  cancel_button = document.createElement('button');
  cancel_button.setAttribute('onclick', remove_storyboard_comment())
  cancel_button.innerHTML= 'Cancel';
  new_comment = document.createElement('textarea');
  comment_container.setAttribute('style','position:absolute;');
  comment_container.style.top = y_pos + 'px';
  comment_container.style.left = x_pos +'px';
  tools_container.appendChild(cancel_button);
  comment_container.appendChild(new_comment);
  comment_container.appendChild(tools_container);
  storyboard_comments.appendChild(comment_container);

}


storyboard_center.addEventListener('click', add_comment, false);


function load_storyboard_frame () {

  console.log(this);
  canvas_img = document.createElement('img');
  storyboard_center.innerHTML = '';
  canvas_img.setAttribute('src', this.firstChild.src);
  canvas_img.setAttribute('style', 'width:100%;height:auto');
  console.log(canvas_img);
  storyboard_center.appendChild(canvas_img);

}


                          function ajax_get_storyboard (order_id) {
                            console.log('ajax_get_storyboard_initiated');

                            var xmlhttp;

                            if (window.XMLHttpRequest)
                              {// code for IE7+, Firefox, Chrome, Opera, Safari
                              xmlhttp=new XMLHttpRequest();
                              }
                            else
                              {// code for IE6, IE5
                              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                              }


                            xmlhttp.onreadystatechange=function()
                              {
                              if (xmlhttp.readyState==4 && xmlhttp.status==200)
                                {

                                    storyboard_nav.innerHTML = ''; 
                                    storyboard_center.innerHTML = '';
                                    result = JSON.parse(xmlhttp.responseText) ;
                                    console.log(result);
                                    for(i=0;i<result.length;i++){
                                      frame_li = document.createElement('li');
                                      frame_img = document.createElement('img');
                                      frame_img.setAttribute('src', result[i]);
                                      frame_img.setAttribute('style', 'width:inherit;height:inherit;')
                                      frame_li.setAttribute('style', 'margin-bottom:5px;border:dotted;width:inherit;height:inherit;');
                                      frame_li.appendChild(frame_img);
                                      storyboard_nav.appendChild(frame_li);
                                      frame_li.addEventListener('click', load_storyboard_frame, false);
                                    }
                                    canvas_img = document.createElement('img');
                                    canvas_img.setAttribute('src', storyboard_nav.firstChild.firstChild.src);
                                  storyboard_center.appendChild(canvas_img);

                                }
                                else{
                                  storyboard_center.innerHTML = 'Loading...' ;
                                }
                              }
                       
                            xmlhttp.open("GET","/get_storyboard/"+ "{{current_user.username}}"+"/" + order_id,true);


                            xmlhttp.send();

                        }


function storyboard_modal (order_id) {

  options = {
            modal: false,
            autoOpen: false,
            resizable : false,
            draggable : true,
            width : 1200,

            show: {
                effect: "explode",
                duration: 700
              },
            hide: {
                effect: "drop",
                duration: 700
              },

            buttons: [ 
                    { text: "Save", class: "btn btn-inverse", click: function() { $( this ).dialog( "close" );} 
                    },
                    { text: "Close", class: "btn btn-primary", click: function() { $( this ).dialog( "close" );} 
                    },
                 ],
            }

 $(function() {
    $( "#storyboard_modal" ).dialog(options);
  });
    console.log('storyboard!!!');

    $( "#storyboard_modal" ).dialog({ title: 'Story-Board' });
    $('#storyboard_modal').dialog( "open" );
    console.log(order_id);

    ajax_get_storyboard(order_id);

}

