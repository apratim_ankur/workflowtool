from django import forms
from request_handler.models import *

class UploadTemplateForm(forms.Form):


	logo = forms.FileField(label='Select a file')

 
	description = forms.CharField(max_length=100, widget=forms.Textarea(attrs={'rows': 6}))

	description = forms.CharField(max_length=300,  widget=forms.Textarea(attrs={'rows': 4}))


	var1 = forms.FileField(label='Select a file')

	var2 = forms.FileField(label='Select a file')

	var3 = forms.FileField(label='Select a file')

	var4 = forms.FileField(label='Select a file')

	var5 = forms.FileField(label='Select a file')

	var6 = forms.FileField(label='Select a file')

	var7 = forms.FileField(label='Select a file')

	var8 = forms.FileField(label='Select a file')

	var9 = forms.FileField(label='Select a file')

	var10 = forms.FileField(label='Select a file')

	var11 = forms.FileField(label='Select a file')




class FeedbackForm(forms.Form):
    rate_types = (
        ('excellent' , 'Excellent'),
        ('very_good' , 'Very Good'),
        ('quite_good' , 'Quite Good'),
        ('somewhat_good' , 'Somewhat Good'),
        ('could_better' , 'Could have been better'),		
        ('need_improve', 'Need to improve a lot')
    )
    
    rate = forms.ChoiceField(choices=rate_types)
    message = forms.CharField(max_length=400, widget=forms.Textarea(attrs={'rows': 6}))
