from django.conf.urls import patterns, include, url, handler404
import request_handler.views
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

# from dajaxice.core import dajaxice_autodiscover, dajaxice_config
# dajaxice_autodiscover()

handler404 = 'request_handler.views.view_404'

urlpatterns = patterns(
    '',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # url(r'^$', request_handler.views.mainpage),
    url(r'^login/(?P<path>.*)$', request_handler.views.login_handler),
    url(r'^signup/(?P<path>.*)/$', request_handler.views.signup), 	
    url(r'^logout/(?P<path>.*)$', request_handler.views.logout_handler),


    url(r'^feedback/$', request_handler.views.feedback),
    # url(r'^subscribe/$', request_handler.views.subscribe), 

    # url(r'^dashboard/$', request_handler.views.dashboard),
    # url(r'^gallery/$', request_handler.views.gallery),

    url(r'^about/$', request_handler.views.about),


    url(r'^get_storyboard/(?P<username>.*)/(?P<order_id>\w+)$', request_handler.views.get_storyboard),



    url(r'^contact/$', request_handler.views.contact),

    url(r'^$', request_handler.views.dashboard), #comment these 2 lines while uncommenting the urls pertaining to bizmovies
    url(r'^individual_order_profile/(?P<username>\w+)/(?P<order_title>\w+)/$', request_handler.views.individual_order_profile),  #comment these 2 lines while uncommenting the urls pertaining to bizmovies 

    url(r'^enquiry_profile/(?P<username>\w+)/(?P<enquiry_title>\w+)/$', request_handler.views.enquiry_profile),

    url(r'^workflowtoolapp/$', request_handler.views.workflowtoolapp),
    url(r'^dashboard/(?P<username>\w+)/$', request_handler.views.dashboard),
    url(r'^storyboard/$', request_handler.views.storyboard_view),

    url(r'^create_enquiry/(?P<enquiry_title>\w+)/(?P<current_user>\w+)/$', request_handler.views.create_enquiry),

    url(r'^create_po/(?P<loggedin_user>\w+)/(?P<enquiry_title>\w+)/$', request_handler.views.create_po),

    # url(r'^workflowtable/video_profile/(?P<video_title>\w+)/$', request_handler.views.video_profile)

    # handler404 = 'request_handler.views.view_404',

   
)


